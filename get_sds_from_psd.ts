import {
  addDistributorAddress,
  downloadFile,
  formatNumber as _formatNumber,
  log,
  readSdsList,
} from "./utils.ts";
import { args, MSDS_LIST, PSD_MSDS_URL } from "./config.ts";

export async function main() {
  const NOT_FOUND = [];
  const langu = args["lang"] || args["l"] || "PL";
  const valArea = args["area"] || args["a"] || "PL";
  const sdsList = await readSdsList(MSDS_LIST);
  const sdsTotal = sdsList.length;
  for await (const [i, number] of sdsList.entries()) {
    log.info(`\n${i + 1}/${sdsTotal}: ${number}`, ["white"]);
    // const formattedNumber = formatNumber(number);
    const sdsUrls = await fetchPsdSdsUrl(number, langu, valArea);
    if (sdsUrls.length === 0) {
      NOT_FOUND.push(number);
      log.error("BRAK!");
    } else {
      const sdsFileNameSuffix = sdsUrls.length > 1 ? "-X.pdf" : ".pdf";
      const sdsFileName =
        `${number}_${langu}`.replace(/_PL$/, "").replaceAll(" ", "_") +
        sdsFileNameSuffix;
      for (const [i, sdsUrl] of sdsUrls.entries()) {
        const filePath = await downloadFile(
          sdsUrl,
          sdsFileName.replace("-X.pdf", `${["a", "b", "c"][i]}.pdf`),
        );
        if (langu === "PL") await addDistributorAddress(filePath);
      }
    }
  }
  if (NOT_FOUND.length > 0) {
    log.info(`\nNie znaleziono:\n${NOT_FOUND.join("\n")}`, ["bold", "bgRed"]);
  }
}

export async function fetchPsdSdsUrl(
  sdsNumber: string,
  langu: string,
  valArea: string,
) {
  const batchId = "batch_" +
    crypto.randomUUID().split("-").splice(1, 3).join("-");
  sdsNumber = sdsNumber.replaceAll(" ", "%20");
  const resp = await fetch(
    "https://psddb.vwgroup.com/sap/opu/odata/vwk/OTS_PSD_SRV/$batch?sap-client=100",
    {
      "credentials": "include",
      "headers": {
        "Content-Type": `multipart/mixed;boundary=${batchId}`,
      },
      "referrer":
        "https://psddb.vwgroup.com/sap/bc/ui5_ui5/vwk/otf_psd_fe/index.html?sap-client=100",
      "body":
        `\r\n--${batchId}\r\nContent-Type: application/http\r\nContent-Transfer-Encoding: binary\r\n\r\nGET MaterialSearchSet?sap-client=100&$filter=ObjectId%20eq%20%27${sdsNumber}%27%20and%20Spras%20eq%20%27${langu}%27%20and%20ApproxObjectId%20eq%20%27${sdsNumber}%27 HTTP/1.1\r\nsap-cancel-on-close: true\r\nsap-contextid-accept: header\r\nAccept: application/json\r\nAccept-Language: pl\r\nDataServiceVersion: 2.0\r\nMaxDataServiceVersion: 2.0\r\n\r\n\r\n--${batchId}\r\nContent-Type: application/http\r\nContent-Transfer-Encoding: binary\r\n\r\nGET ResultListSet?sap-client=100&$skip=0&$top=100&$orderby=Matnr%20asc&$filter=Matnr%20eq%20%27${sdsNumber}%27%20and%20Ldepid_Ident%20eq%20%27MSDS%27%20and%20Langu%20eq%20%27${langu}%27%20and%20Rvlid%20eq%20%27${valArea}%27&$inlinecount=allpages HTTP/1.1\r\nsap-cancel-on-close: true\r\nsap-contextid-accept: header\r\nAccept: application/json\r\nAccept-Language: pl\r\nDataServiceVersion: 2.0\r\nMaxDataServiceVersion: 2.0\r\n\r\n\r\n--${batchId}--\r\n`,
      "method": "POST",
      "mode": "cors",
    },
  );
  const sdsUrls = [];
  try {
    const text = await resp.text();
    const json = JSON.parse(text.trim().split("\n").at(-2) as string);
    const results = json.d.results;
    for (const result of results) {
      const subId = result["Subid"];
      const sbgvId = result["Sbgvid"];
      sdsUrls.push(PSD_MSDS_URL(sdsNumber, subId, langu, valArea, sbgvId));
    }
  } catch (err) {
    log.critical(`${sdsNumber}: Nie można pobrać karty\n${err}`);
  }
  if (args.r || args.reverse) return sdsUrls.reverse();
  return sdsUrls;
}

if (import.meta.main) main();
