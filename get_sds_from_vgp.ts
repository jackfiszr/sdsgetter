// @ts-nocheck https://github.com/b-fuze/deno-dom/issues/4
import { args, VGP_BASE_URL, VGP_MSDS_URL } from "./config.ts";
import { DOMParser } from "./deps.ts";
import { downloadFile } from "./utils.ts";

export async function main() {
  const response = await fetch(VGP_MSDS_URL);
  const html = await response.text();
  const parser = new DOMParser();
  const document = parser.parseFromString(html, "text/html");

  const table = document?.querySelector("table#datatable_full");
  const pdfLinks = table?.querySelectorAll("a[href$='.pdf']");

  const uniquePdfLinks = Array.from(new Set(pdfLinks || []));
  const uniquePdfHrefs = Array.from(
    new Set(uniquePdfLinks.map((link) => link.getAttribute("href") || "")),
  );

  uniquePdfHrefs.sort((a, b) => {
    return a.split("/").pop()!.localeCompare(b.split("/").pop()!);
  });

  const totalFiles = uniquePdfHrefs.length;
  let downloadedFiles = 0;

  const { _: specifiedFiles } = args;

  const specifiedFilesSet = new Set(
    specifiedFiles.map((arg: string) => {
      const trimmed = arg.trim().replaceAll(" ", "_");
      const lastChar = trimmed.at(-1);
      const transformed =
        lastChar && ["a", "b", "c", "d", "e", "f"].includes(lastChar)
          ? trimmed.slice(0, -1).toUpperCase() + lastChar
          : trimmed.toUpperCase();
      return transformed;
    }),
  );

  for (const href of uniquePdfHrefs) {
    const pdfUrl = new URL(href, VGP_BASE_URL).toString(); // Convert to absolute URL
    const filename = href.split("/").pop() || "";
    // Skip if specified files are provided and the current file is not in the list
    if (
      specifiedFilesSet.size > 0 &&
      !specifiedFilesSet.has(filename.split(".", 1)[0])
    ) {
      continue;
    }

    downloadedFiles += 1;
    console.log(
      `\n${
        String(downloadedFiles).padStart(3, "0")
      }/${totalFiles}: ${filename}`,
    );
    await downloadFile(pdfUrl, filename);
  }
}

if (import.meta.main) main();
