import {
  args,
  MSDS_DIR,
  VGP_BASE_URL,
  VGP_MSDS_API_ENDPOINT,
} from "./config.ts";
import {
  basename,
  Buffer,
  ensureDir,
  existsSync,
  join,
  Parser,
} from "./deps.ts";
import { dlog, sdsToTxt as msdsToTxt } from "./utils.ts";

export async function main() {
  const response = await fetch(VGP_MSDS_API_ENDPOINT);
  const msdsJson = await response.json();
  const numberOfProducts = msdsJson.length;
  const allMmsdsUrls = [];
  await ensureDir(MSDS_DIR);
  for (const msds of msdsJson) {
    const anchorEl = msds["KARTA"];
    const msdsPaths = getHrefs(anchorEl);
    const msdsUrls = msdsPaths.map((msdsPath) => VGP_BASE_URL + msdsPath);
    allMmsdsUrls.push(...msdsUrls);
  }
  for (const msdsUrl of allMmsdsUrls) {
    const msdsPath = await fetchMsds(msdsUrl);
    if (msdsPath && args.parse) {
      await msdsToTxt(msdsPath);
    }
  }
  const allMsdsCount = allMmsdsUrls.length;
  dlog({
    color: "magenta",
    title: "LICZBA",
    mainMsg: `Kart charakterystyki ${allMsdsCount}`,
    subMsg: `dla produktów: ${numberOfProducts}`,
  });
}

async function fetchMsds(msdsUrl: string) {
  const filename = basename(msdsUrl);
  const filepath = join(MSDS_DIR, filename);
  if (existsSync(filepath)) {
    dlog({
      color: "cyan",
      title: "JUŻ ISTNIEJE",
      mainMsg: filepath,
    });
    return filepath;
  }
  dlog({
    color: "yellow",
    title: "Pobieranie",
    mainMsg: msdsUrl,
  });
  const response = await fetch(msdsUrl);
  const blob = await response.blob();
  const buffer = await blob.arrayBuffer();
  const unit8arr = new Buffer(buffer).bytes();
  Deno.writeFileSync(filepath, unit8arr);
  dlog({
    color: "green",
    title: "Zapisano",
    mainMsg: filepath,
  });
  return filepath;
}

function getHrefs(anchorEl: string) {
  const results: string[] = [];
  const parser = new Parser({
    onopentag(tagname: string, attribs: { [key: string]: string }) {
      if (tagname === "a") {
        results.push(attribs.href);
      }
    },
  });
  parser.write(anchorEl);
  parser.end();
  return results;
}

if (import.meta.main) main();
