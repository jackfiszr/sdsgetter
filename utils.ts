export {
  dlog,
  getLogger,
  pdfToTxt as sdsToTxt,
  runCmd,
  txtToCleanArr,
} from "https://bitbucket.org/jackfiszr/utils/raw/v1.5.7/mod.ts";
import { dlog, getLogger, txtToCleanArr } from "./utils.ts";
import { args, MSDS_DIR, os, PSD_BASE_URL, VGP_BASE_URL } from "./config.ts";
import {
  appVer,
  basename,
  Buffer as _Buffer,
  ensureDir,
  existsSync,
  fontkit,
  join,
  PDFDocument,
} from "./deps.ts";

export const log = await getLogger();

export function intro() {
  const introStr = `
         _________                   __________
  _____________  /_____________ _______  /__  /_____________
  __  ___/  __  /__  ___/_  __ \`/  _ \\  __/  __/  _ \\_  ___/
  _(__  )/ /_/ / _(__  )_  /_/ //  __/ /_ / /_ /  __/  /
  /____/ \\__,_/  /____/ _\\__, / \\___/\\__/ \\__/ \\___//_/ ${appVer}
                        /____/
`;
  log.info(introStr);
}

export function formatNumber(number: string) {
  number = number.trim().replace(/[^\w]/g, "");
  const spacer = " ";
  let result;
  const numberLen = number.length;
  switch (numberLen) {
    case 14:
      result = number;
      break;
    case 13:
      result = number.substr(0, 10) + spacer + number.substr(10);
      break;
    case 12:
      result = number.substr(0, 9) + spacer + spacer + number.substr(9);
      break;
    case 11:
      result = number;
      break;
    case 10:
      result = number;
      break;
    case 9:
      result = number[0] + spacer + spacer + number.substr(1);
      break;
    case 8:
      result = number[0] + spacer + number.substr(1);
      break;
    case 7:
      result = number[0] + spacer + spacer + number.substr(1);
      break;
  }
  dlog({
    color: "yellow",
    title: number,
    mainMsg: `\t${result}`,
  });
  return result;
}

export async function readSdsList(fileName: string): Promise<string[]> {
  let sdsListTxt: string, sdsListArr: string[] = [];
  if (args._.length) sdsListTxt = args._.join("\n");
  else {
    try {
      sdsListTxt = (await Deno.readTextFile(fileName)).trim();
    } catch (e) {
      dlog({
        color: "red",
        title: "BRAK LISTY",
        mainMsg: `Nie można otworzyć pliku ${fileName}`,
        subMsg: `${e}`,
      });
      return sdsListArr;
    }
  }
  if (sdsListTxt.length === 0) {
    dlog({
      color: "red",
      title: "BRAK LISTY",
      mainMsg:
        `Podaj numery katalogowe produktów w pliku ${fileName} lub jako rozdzielone przecinkami argumenty do polecenia sds psd\n`,
    });
    return sdsListArr;
  } else {
    dlog({
      color: "bgBlue",
      title: "ROZPOCZĘTO",
      mainMsg: "Wczytywanie listy numerów katalogowych...",
      subMsg: new Date().toLocaleTimeString(),
    });
    sdsListArr = txtToCleanArr(sdsListTxt).map((e) => e.replaceAll("-", ""));
    const unverifiedSdsCount = sdsListArr.length;
    const verifiedSdsCount = sdsListArr.length;
    const discardedSdsCount = unverifiedSdsCount - verifiedSdsCount;
    dlog({
      color: "blue",
      title: "WCZYTANO listę numerów produktów",
      mainMsg: `${verifiedSdsCount} z ${unverifiedSdsCount} pozycji`,
      subMsg: `pozycji odrzucownych jako błędne: ${discardedSdsCount}\n`,
    });
  }
  return sdsListArr.map((e) => e.replaceAll("_", " "));
}

export async function downloadFile(sdsUrl: string, fileName?: string) {
  let dirName = MSDS_DIR;
  if (sdsUrl.includes(PSD_BASE_URL)) dirName += "_PSD";
  if (sdsUrl.includes(VGP_BASE_URL)) dirName += "_VGP";
  await ensureDir(dirName);
  if (!fileName) fileName = basename(sdsUrl);
  const filePath = join(dirName, fileName);
  if (existsSync(filePath)) {
    log.info(`JUŻ ISTNIEJE: ${filePath}`, ["bold", "cyan"]);
    return filePath;
  }
  log.info(`Pobieranie: ${sdsUrl}`, ["bold", "yellow"]);
  // const response = await fetch(sdsUrl);
  // const blob = await response.blob();
  // const buffer = await blob.arrayBuffer();
  // const unit8arr = new Buffer(buffer).bytes();
  const pdfResponse = await fetch(sdsUrl);
  const pdfData = new Uint8Array(await pdfResponse.arrayBuffer());
  await Deno.writeFile(filePath, pdfData);
  log.info(`Zapisano: ${filePath}`, ["bold", "green"]);
  return filePath;
}

export async function addDistributorAddress(pdfPath: string) {
  if (!pdfPath.toLowerCase().endsWith(".pdf")) {
    log.error("PRZERYWAM: Plik powinien mieć rozszerzenie *.pdf");
    return;
  }
  log.info("Dodawanie adresu dystrybutora...", ["yellow"]);
  const distributorInfo = `Dystrybutor w Polsce:

Firma:
Volkswagen Group Polska Sp. z o.o.
ul. Krańcowa 44
61-037 Poznań
Numer telefonu:
+48 61 62 73 000
Adres e-mail osoby
odpowiedzialnej za SDS:
karty.charakterystyki@vw-group.pl

Numer telefonu alarmowego:
+48 61 62 73 000 (8:00-16:00)
Europejski numer alarmowy: 112`;

  const oldPdfPath = pdfPath.replace(".pdf", "(no-distributor-address).pdf");
  Deno.renameSync(pdfPath, oldPdfPath);
  const oldPdfBytes = Deno.readFileSync(oldPdfPath);
  const pdfDoc = await PDFDocument.load(oldPdfBytes, {
    ignoreEncryption: true,
  });
  pdfDoc.registerFontkit(fontkit);
  const fontPath = os === "windows"
    ? "C:\\Windows\\Fonts\\arial.ttf"
    : `${Deno.env.get("HOME")}/.fonts/arial.ttf`;
  const arial = await pdfDoc.embedFont(
    Deno.readFileSync(fontPath),
  );
  const pages = pdfDoc.getPages();
  const fontSize = args.f || args["font-size"] || 9;
  pages.forEach((page, i) => {
    if (i === 0) {
      page.drawText(distributorInfo, {
        x: args.x || args["address-position-x"] || 435,
        y: args.y || args["address-position-y"] || 410,
        font: arial,
        size: fontSize,
        lineHeight: fontSize * 1.1,
      });
    }
  });
  const newPdfBytes = await pdfDoc.save();
  Deno.writeFileSync(pdfPath, newPdfBytes);
  Deno.removeSync(oldPdfPath);
  log.info("OK.", ["green"]);
}
