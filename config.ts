import { join, parseArgs } from "./deps.ts";

export const os = Deno.build.os;
export const args = (() => {
  const parsedArgs = parseArgs(Deno.args);
  const positionalArgs = parsedArgs._ as string[];
  if (["psd", "vag", "vgp", "dpl"].includes(positionalArgs[0])) {
    positionalArgs.shift(); // Omit the first positional argument (the script name)
  }
  return parsedArgs;
})();

export const MSDS_DIR = join(Deno.cwd(), "SDS");
export const SOURCE_DIR = join(Deno.cwd(), "MSDS_source");

export const VGP_BASE_URL = "https://www.vw-group.pl";
export const VGP_MSDS_URL =
  `${VGP_BASE_URL}/volkswagen-group-polska,homologacja-i-ekologia.html#karty`;

export const VAG_BASE_URL = "http://msds.cpn.vwg/ehswww/msds/page3/pl";
export const VAG_MSDS_URL = (MSDS_NUMBER: string) =>
  `${VAG_BASE_URL}/result.jsp?P_LANGU=E&P_SYS=1&P_SSN=265&C001=MSDS&C002=*&C003=L&C004=&C005=*&C013=${MSDS_NUMBER}&C014=`;

export const PSD_BASE_URL = os === "linux"
  ? "https://psddb.vwgroup.com"
  : "https://psddb.cpn.vwg";
export const PSD_ODATA_URL =
  `${PSD_BASE_URL}/sap/opu/odata/VWK/OTS_PSD_CPN_SRV`;
export const PSD_BATCH_URL = `${PSD_ODATA_URL}/$batch?sap-client=100`;
export const PSD_MSDS_URL = (
  matNr: string,
  subId: string,
  langu = "PL",
  valArea = "PL",
  sbgvId = "SDB_GWU_PL",
) =>
  `${PSD_ODATA_URL}/FileDisplaySet(Matnr='${matNr}',Ldepid_Ident='MSDS',Langu='${langu}',Valarea='${valArea}',Subid='${subId}',Sbgvid='${sbgvId}')/$value#view=FitH`;

export const MSDS_LIST = join(Deno.cwd(), "lista_sds.txt");
