import { MSDS_DIR } from "./config.ts";
import { log, sdsToTxt } from "./utils.ts";
import { copy, ensureDir, existsSync, expandGlob, join } from "./deps.ts";

const directory = MSDS_DIR + "_VGP";
const noDistributorDirectory = directory + "_without_distributor";
const NOT_CONVERTED = [];
const WITHOUT_DISTRIBUTOR = [];

const globString = join(directory, "*.pdf");
for await (const pdf of expandGlob(globString)) {
  const pdfPath = join(directory, pdf.name);
  log.info(`${pdfPath} ==> *.txt`);
  const txtPath = pdfPath.replace(".pdf", ".txt");
  try {
    await sdsToTxt(pdfPath);
  } catch (e) {
    log.error(e);
  }
  if (existsSync(txtPath)) {
    const txtContent = await Deno.readTextFile(txtPath);
    const searchStrings = ["volkswagen group polska", "krańcowa"];
    if (!searchStrings.some((str) => txtContent.toLowerCase().includes(str))) {
      WITHOUT_DISTRIBUTOR.push(pdf.name);
      await ensureDir(noDistributorDirectory);
      await copy(pdfPath, pdfPath.replace(directory, noDistributorDirectory));
    }
  } else {
    NOT_CONVERTED.push(pdf.name);
    await ensureDir(noDistributorDirectory);
    await copy(pdfPath, pdfPath.replace(directory, noDistributorDirectory));
  }
}
log.info("\nWYNIKI:", ["bgMagenta"]);
log.info(`Nieprzekonwertowane pliki:\n${NOT_CONVERTED.join("\n")}`, ["red"]);
log.info(`Karty bez adresu dystrybutora:\n${WITHOUT_DISTRIBUTOR.join("\n")}`, [
  "yellow",
]);
