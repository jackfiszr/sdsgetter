import { args, os } from "./config.ts";
import { appVer, Command, Select } from "./deps.ts";
import { main as getSdsFromPsd } from "./get_sds_from_psd.ts";
// import { main as getSdsFromVag } from "./get_sds_from_vag.ts";
import { main as getSdsFromVgp } from "./get_sds_from_vgp.ts";
import { main as addDistributorInfo } from "./add_distributor_info.ts";
import { intro, runCmd } from "./utils.ts";

async function main() {
  if (!args.V) intro();
  await new Command()
    // Main command.
    .name("sds")
    .version(appVer).meta("deno", Deno.version.deno)
    .description(`Narzędzie do pobierania kart charakterystyki
Zgłasznie błędów: https://bitbucket.org/jackfiszr/sdsgetter/jira
© 2020-2022 Jacek Fiszer, MIT License`)
    .action(async function () {
      this.showHelp();
      await selectCmd();
    })
    /* VAG command.
    .command(
      "vag",
      "Pobranie kart charakterystyki z systemu EH&S (deprecated).",
    ).action(() => {
      // getSdsFromVag();
      console.log("System EH&S nie jest już dostępny.");
    }) */
    // PSD command.
    .command(
      "psd [indeksy...]",
      "Pobranie kart charakterystyki z systemu PSD On-Demand.",
    ).option(
      "-l, --lang <język>",
      "Wybór języka karty (dwuliterowy kod 'PL', 'EN', 'DE', itd.)",
    ).option(
      "-a, --area <obszar>",
      "Wybór obszaru obowiązywania karty (kod 'PL', 'GB', 'US', itd.)",
    ).option(
      "-r, --reverse",
      "Odwrócenie numeracji składników produktów wielokomponentowych",
    ).option(
      "-x, --address-position-x <koordynat>",
      "Pozycja adresu dystrybutora względem osi x (w poziomie)",
    ).option(
      "-y, --address-position-y <koordynat>",
      "Pozycja adresu dystrybutora względem osi y (w pionie)",
    ).option(
      "-f, --font-size <liczba>",
      "Wielkość czcionki użytej do dopisania adresu dystrybutora",
    ).action(getSdsFromPsd)
    // VGP command.
    .command(
      "vgp [sds_id...]",
      "Pobranie kart charakterystyki ze strony firmowej VGP.",
    ).action(getSdsFromVgp)
    // DIST command.
    .command(
      "dpl [pliki...]",
      "Dodawanie adresu i telefonu dystrybutora PL do karty.",
    ).option(
      "-x, --address-position-x <koordynat>",
      "Pozycja adresu dystrybutora względem osi x (w poziomie)",
    ).option(
      "-y, --address-position-y <koordynat>",
      "Pozycja adresu dystrybutora względem osi y (w pionie)",
    ).option(
      "-f, --font-size <liczba>",
      "Wielkość czcionki użytej do dopisania adresu dystrybutora",
    ).action(addDistributorInfo)
    .parse(Deno.args);
}

async function selectCmd() {
  const cmdStr = await Select.prompt({
    message: "Wybierz jedno z popularnych poleceń (↓d ↑u ⏎):",
    options: [
      // { name: "sds vag", value: "sds vag" },
      { name: "sds psd", value: "sds psd" },
      { name: "sds vgp", value: "sds vgp" },
      { name: "sds dpl", value: "sds dpl" },
      Select.separator("-------"),
      { name: "exit", value: "exit" },
    ],
  });
  if (cmdStr === "exit") return;
  try {
    await runCmd(cmdStr);
  } catch (err) {
    if (os === "windows") {
      runCmd(cmdStr.replace("sds", "sds.cmd"));
    } else {
      console.error(err);
    }
  }
}

if (import.meta.main) main();
