import { args } from "./config.ts";
import { addDistributorAddress, log } from "./utils.ts";

export async function main() {
  const { _: sdsListFromArgs } = args;
  const sdsList = sdsListFromArgs.length ? sdsListFromArgs : listPdfsInCwd();
  const sdsListLength = sdsList.length;
  const sdsListLengthDigits = String(sdsListLength).length;
  if (sdsListLength) {
    for (const [i, sds] of sdsList.entries()) {
      const filepath = String(sds);
      log.info(
        `${
          String(i + 1).padStart(sdsListLengthDigits, "0")
        }/${sdsListLength}\t${filepath}`,
        ["white"],
      );
      await addDistributorAddress(filepath);
    }
  } else {
    log.error("Należy podać ścieżkę do pliku\nUsage: sds dpl [path]");
  }
}

function listPdfsInCwd() {
  return Array.from(Deno.readDirSync(".")).map((e) => e.name).filter((n) =>
    n.endsWith(".pdf")
  );
}

if (import.meta.main) main();
