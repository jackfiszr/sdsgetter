```
       _________                   __________
_____________  /_____________ _______  /__  /_____________
__  ___/  __  /__  ___/_  __ `/  _ \  __/  __/  _ \_  ___/
_(__  )/ /_/ / _(__  )_  /_/ //  __/ /_ / /_ /  __/  /
/____/ \__,_/  /____/ _\__, / \___/\__/ \__/ \___//_/
                      /____/
```

© 2020-2022 Jacek Fiszer, MIT License

Current version: 0.0.16

# Table of Contents

[TOC]

# Install Deno

Shell (Mac, Linux):

    curl -fsSL https://deno.land/x/install/install.sh | sh

PowerShell (Windows):

    iwr https://deno.land/x/install/install.ps1 -useb | iex

For more details go to [deno.land](https://deno.land/)

# Install the sds command

    deno install -f --check --allow-env --allow-read --allow-write --allow-run --allow-net -n sds https://bitbucket.org/jackfiszr/sdsgetter/raw/0.0.16/main.ts

or compile to binary:

    deno compile --check --allow-env --allow-read --allow-write --allow-run --allow-net --output sds https://bitbucket.org/jackfiszr/sdsgetter/raw/0.0.16/main.ts

# Usage

Start with:

    sds --help

or simply:

    sds
