import { MSDS_LIST, SOURCE_DIR, VAG_BASE_URL, VAG_MSDS_URL } from "./config.ts";
import { Buffer, ensureDir, existsSync, join, Parser } from "./deps.ts";
import { dlog, readSdsList as readMsdsList } from "./utils.ts";

const NOT_FOUND: string[] = [];

export async function main() {
  const msdsList = await readMsdsList(MSDS_LIST);
  const msdsCount = msdsList.length;
  const allDownloadedMsdss = [];
  if (msdsCount > 0) {
    // await checkIfFilesExist(msdsList)
    for (const [i, msdsNumber] of msdsList.entries()) {
      dlog({
        color: "inverse",
        title: `${i + 1} z ${msdsCount}`,
        mainMsg: "",
      });
      const downloadedMsdss = await downloadMsds(msdsNumber);
      if (downloadedMsdss) {
        allDownloadedMsdss.push(downloadedMsdss);
      }
    }
  }
  dlog({
    color: "magenta",
    title: "RAPORT",
    mainMsg:
      `Znaleziono ${allDownloadedMsdss.length} z ${msdsCount} wyszukiwanych kart charakterystyki`,
    subMsg: `nie znaleziono:\n${NOT_FOUND.join("\n")}`,
  });
}

async function downloadMsds(msdsNumber: string) {
  const downloadedMsdss: string[] = [];
  dlog({
    color: "yellow",
    title: msdsNumber,
    mainMsg: `Wyszukiwanie na stronie EH&S Web Interface`,
    subMsg: VAG_MSDS_URL(msdsNumber),
  });
  const waitUrls = await getWaitUrls(msdsNumber);
  const numerOfFoundMsdss = waitUrls.length;
  if (numerOfFoundMsdss > 0) {
    dlog({
      color: "green",
      title: msdsNumber,
      mainMsg: `Znaleziono kart ${waitUrls.length}`,
      subMsg: waitUrls.join("\n > "),
    });
    const reportUrls = waitUrls.map(async (waitUrl) =>
      await reportUrlFrom(waitUrl)
    );
    await Promise.all(reportUrls).then(async (results) => {
      dlog({
        color: "green",
        title: msdsNumber,
        mainMsg: `Wygenerowano łącza do kart`,
        subMsg: results.join("\n > "),
      });
      for (const reportUrl of results) {
        const downloadadMsds = await fetchMsds(reportUrl, msdsNumber);
        downloadedMsdss.push(downloadadMsds);
      }
    });
    return downloadedMsdss;
  } else {
    dlog({
      color: "bgRed",
      title: msdsNumber,
      mainMsg: `Nie znaleziono karty charakterystyki`,
    });
    NOT_FOUND.push(msdsNumber);
  }
}

async function getWaitUrls(msdsNumber: string) {
  const response = await fetch(VAG_MSDS_URL(msdsNumber));
  const html = await response.text();
  const hrefList = getAttrIncluding({
    html: html,
    tag: "a",
    attr: "href",
    text: "wait.jsp",
  });
  const waitUrls = hrefList.map((href) => VAG_BASE_URL + href);
  return waitUrls;
}

async function reportUrlFrom(waitUrl: string) {
  const response = await fetch(waitUrl);
  const html = await response.text();
  const reportUrl = VAG_BASE_URL +
    getAttrIncluding({
      html: html,
      tag: "meta",
      attr: "content",
      text: "report.jsp",
    })[0].split("; URL=")[1].trim();
  return reportUrl;
}

async function fetchMsds(msdsUrl: string, msdsNumber: string) {
  await ensureDir(SOURCE_DIR);
  const filename = `${msdsNumber.replace(/ /g, "_")}.pdf`;
  const filepath = join(SOURCE_DIR, filename);
  if (existsSync(filepath)) {
    dlog({
      color: "red",
      title: msdsNumber,
      mainMsg: "Plik już istnieje",
      subMsg: filepath,
    });
    return filepath;
  }
  dlog({
    color: "yellow",
    title: msdsNumber,
    mainMsg: "Pobieranie pliku",
    subMsg: msdsUrl,
  });
  const response = await fetch(msdsUrl);
  const blob = await response.blob();
  const buffer = await blob.arrayBuffer();
  const unit8arr = new Buffer(buffer).bytes();
  Deno.writeFileSync(filepath, unit8arr);
  dlog({
    color: "green",
    title: msdsNumber,
    mainMsg: "Zapisano na dysku",
    subMsg: filepath,
  });
  return filepath;
}

function getAttrIncluding(
  opts: { html: string; tag: string; attr: string; text: string },
) {
  const results: string[] = [];
  const parser = new Parser({
    onopentag(tagname: string, attribs: { [key: string]: string }) {
      if (tagname === opts.tag) {
        const prop = attribs[opts.attr];
        if (prop && prop.includes(opts.text)) {
          results.push(prop);
        }
      }
    },
  });
  parser.write(opts.html);
  parser.end();
  return results;
}

if (import.meta.main) main();
